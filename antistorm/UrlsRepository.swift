//
//  UrlsRepository.swift
//  antistorm
//
//  Created by Grzegorz Moscichowski on 17/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import Foundation
import Alamofire

class Networking {
    static let manager: SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "antistorm.eu": .disableEvaluation,
            "wykop.pl": .disableEvaluation,
            "https://wykop.pl": .disableEvaluation
        ]
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        return Alamofire.SessionManager(configuration: configuration,
                                 serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }()
    
    func test() {
        Networking.manager.request("https://localhost:3000/", method: .get, parameters: ["a":3])
//            method: "https://localhost:3000/", parameters: nil)
        
    }
}

struct UrlsRepository {
    func requestUrls(completion: @escaping (String) -> Void) {
//        Alamofire.Manager.sharedInstance.session.serverTrustPolicy = myCustomPolicy
        Alamofire.request("https://antistorm.eu/ajaxPaths.php?lastTimestamp=0&type=radar").validate().responseString { (response) in
            print(response)
            completion("")
        }
    }
}
