//
//  DateStringGenerator.swift
//  antistormTests
//
//  Created by Grzegorz Moscichowski on 16/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import XCTest

class DateStringGeneratorTests: XCTestCase {

    func testExample() {
        let result = String.composeArrays(elements:["1","2"], separators:["a"])
        XCTAssertEqual(result, "1a2")
    }
    
    func testExample2() {
        let result = String.composeArrays(elements:["1","2","3"], separators:["a","b"])
        XCTAssertEqual(result, "1a2b3")
    }

}
