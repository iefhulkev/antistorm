//
//  UrlGeneratorTests.swift
//  antistormTests
//
//  Created by Grzegorz Moscichowski on 14/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import XCTest

class UrlGeneratorTests: XCTestCase {
    var generator:UrlGenerator?
    
    override func setUp() {
        generator = UrlGenerator(date: exampleDate())
    }
    
    func exampleDate() -> Date {
        // 2019-05-14 21:51:11 +0000
        return Date(timeIntervalSince1970: 1557870671)
    }

    //"https://antistorm.eu/visualPhenom/20190513.1048-radar-visualPhenomenon.png"

    func testCalculationOfLast8() {
        XCTAssertEqual(generator?.distanceFrom8OnLastDigit(10), 2)
        XCTAssertEqual(generator?.distanceFrom8OnLastDigit(8), 0)
        XCTAssertEqual(generator?.distanceFrom8OnLastDigit(0), 2)
    }

    func testStringGeneration() {
        XCTAssertEqual(generator?.getDateDotTimeString(forDate: exampleDate()), "20190514.2151")
        XCTAssertEqual(generator?.getVisualPhenomenonDateString(), "20190514.2148")
        XCTAssertEqual(generator?.getProbabilitiesDateString(), "2019.05.14/23-10")
    }
}
