import Foundation

extension String {

    static func composeArrays(elements:[String], separators:[String]) -> String {
        var result = ""
        for i in 0..<elements.count {
            result = "\(result)\(elements[i])"
            if separators.count > i {
                result = result + separators[i]
            }
        }
        return result
    }
}
