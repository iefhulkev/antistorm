//
//  ViewController.swift
//  antistorm
//
//  Created by Grzegorz Moscichowski on 14/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

enum MapMode:String {
    case rain = "radar"
    case storm = "storm"
}

struct UrlsRepo {
    var mode = MapMode.rain
    var rainUrls:[UrlsModel]?
    var stormUrls:[UrlsModel]?
    
    func getModelFor(index:Int) -> UrlsModel? {
        switch mode {
        case .rain:
            return rainUrls?[index]
        case .storm:
            return stormUrls?[index]
        }
    }
}

class ViewController: UIViewController {
    
    @IBOutlet weak var map: UIImageView!
    @IBOutlet weak var clouds: UIImageView!
    @IBOutlet weak var probalility: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var modeSwitch: UISwitch!
    
//    var urlsData: [UrlsModel]?
    var selectedModel: Int = 0
//    var mode = MapMode.rain
    var urlsRepo = UrlsRepo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mapUrl = URL(string:"https://antistorm.eu/map/final-map.png")
        map.sd_setImage(with: mapUrl, completed: nil)
        
        Alamofire.request("https://antistorm.eu/ajaxPaths.php", method: .get, parameters: ["lastTimestamp":0, "type":"radar"]).validate().responseString { (response) in
            self.urlsRepo.rainUrls = UrlsRepoResponseParser.urlsModels(fromResponse: response.result.value ?? "")
            self.updateImages()
//            self.setupWithUrls(urls: UrlsRepoResponseParser.urlsModels(fromResponse: response.result.value ?? ""))
        }
        Alamofire.request("https://antistorm.eu/ajaxPaths.php", method: .get, parameters: ["lastTimestamp":0, "type":"storm"]).validate().responseString { (response) in
            self.urlsRepo.stormUrls = UrlsRepoResponseParser.urlsModels(fromResponse: response.result.value ?? "")
            self.updateImages()
        }
        
    }
    
    @IBAction func valueChanged(_ sender: UISlider) {
        selectedModel = Int(sender.value)
        updateImages()
    }
    
//    func setupWithUrls(urls:[UrlsModel]?) {
//        urlsData = urls
//        updateImages()
//    }
    
    func updateImages() {
        let model = urlsRepo.getModelFor(index: selectedModel)
        clouds.sd_setImage(with: model?.visualPhenomenon, completed: nil)
        probalility.sd_setImage(with: model?.probablitity, completed: nil)
        date.text = model?.time.description
    }

    @IBAction func switchTap(_ sender: Any) {
        let newState = !modeSwitch.isOn
        modeSwitch.setOn(newState, animated: true)
        urlsRepo.mode = newState ? .rain : .storm
        updateImages()
    }
    
}

