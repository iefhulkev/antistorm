//
//  UrlsRepoResponseParser.swift
//  antistorm
//
//  Created by Grzegorz Moscichowski on 18/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import Foundation

struct UrlsModel {
    let time:Date
    let visualPhenomenon:URL
    let probablitity:URL
    
    init?(components:UrlComponentsDataModel) {
        time = Date(timeIntervalSince1970: TimeInterval( components.timestamp))
        let vpUrl = URL(string:"https://antistorm.eu/visualPhenom/\(components.frontFileName)-radar-visualPhenomenon.png")
        let probUrl = URL(string: "https://antistorm.eu/archive/\(components.folderName)/\(components.filename)-radar-probabilitiesImg.png")
        if let vpUrl = vpUrl, let probUrl = probUrl {
            visualPhenomenon = vpUrl
            probablitity = probUrl
            return
        }
        return nil
    }
}

struct UrlComponentsDataModel {
    let timestamp:Int
    let folderName:String
    let filename:String
    let frontFileName:String
    let stormActivity:Int
    
    init?(timestamp:String?, folderName:String?, filename:String?, frontFileName:String?, stormActivity:String?) {
        if let timestamp = timestamp, let folderName = folderName, let filename = filename, let frontFileName = frontFileName, let stormActivity = stormActivity {
            if let timestamp = Int(timestamp), let stormActivity = Int(stormActivity) {
                self.timestamp = timestamp
                self.folderName = folderName
                self.filename = filename
                self.frontFileName = frontFileName
                self.stormActivity = stormActivity
                return
            }
        }
        return nil
    }
}

struct PropertyResponseModel {
    let propertyName:String
    let values:[String]
}

struct UrlsRepoResponseParser {
    let input:String
    
    static func urlsModels(fromResponse response:String) -> [UrlsModel] {
        let parser = UrlsRepoResponseParser(input: response)
        return parser.extractUrls()
    }
    
    func extractUrls() -> [UrlsModel] {
        let components = extractUrlComponents()
        return components.compactMap{UrlsModel(components: $0)}
    }
    
    func extractUrlComponents() -> [UrlComponentsDataModel] {
        let properties = input.components(separatedBy: "<br>")
        let rawData = properties.compactMap(extractPropertyData)
        var result = [UrlComponentsDataModel]()
        for i in 0..<10 {
            if let model = extractModelOfIndex(index: i, from: rawData) {
                result.append(model)
            }
        }
        return result
    }
    
    fileprivate func extractPropertyData(input:String) -> PropertyResponseModel? {
        let result = input.components(separatedBy: ":")
        if result.count == 2 {
            return PropertyResponseModel(propertyName: result[0], values: result[1].components(separatedBy: ","))
        }
        return nil
    }
    
    fileprivate func extractModelOfIndex(index:Int, from responseModels:[PropertyResponseModel]) -> UrlComponentsDataModel? {
        return UrlComponentsDataModel(
            timestamp: extractValue(forPropertyName: "timestamp", index: index, from: responseModels),
            folderName: extractValue(forPropertyName: "nazwa_folderu", index: index, from: responseModels),
            filename: extractValue(forPropertyName: "nazwa_pliku", index: index, from: responseModels),
            frontFileName: extractValue(forPropertyName: "nazwa_pliku_front", index: index, from: responseModels),
            stormActivity: extractValue(forPropertyName: "aktywnosc_burz", index: index, from: responseModels))
    }
    
    fileprivate func extractValue(forPropertyName name:String, index:Int, from responseModels:[PropertyResponseModel]) -> String? {
        for model in responseModels {
            if model.propertyName == name {
                if model.values.count > index {
                    return model.values[index]
                }
            }
        }
        return nil
    }
}
