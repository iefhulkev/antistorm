//
//  NameGenerator.swift
//  antistorm
//
//  Created by Grzegorz Moscichowski on 14/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import Foundation
//"https://antistorm.eu/visualPhenom/20190513.1048-radar-visualPhenomenon.png"
// https://antistorm.eu/archive/2019.5.16/22-20-radar-probabilitiesImg.png
struct UrlGenerator {
    let date:Date
    
    func dateComponents(forDate date:Date) -> [Int] {
        let calendar = Calendar.current
        var result = [Int]()
        result.append(calendar.component(.year, from: date))
        result.append(calendar.component(.month, from: date))
        result.append(calendar.component(.day, from: date))
        result.append(calendar.component(.hour, from: date))
        result.append(calendar.component(.minute, from: date))
        return result
    }
    
    func dateComponentStrings(components:[Int]) -> [String] {
        return components.map{(element) in
            if element < 10 {
                return "0\(element)"
            }
            return element.description
        }
    }
    
    func getVisualPhenomenonUrl() -> URL? {
        let urlString = "https://antistorm.eu/visualPhenom/\(getVisualPhenomenonDateString())-radar-visualPhenomenon.png"
        return URL(string: urlString)
    }
    
    func getVisualPhenomenonDateString() -> String {
        let calendar = Calendar.current
        let minutes = calendar.component(.minute, from: date)
        let lastMapMunutesAgo = distanceFrom8OnLastDigit(minutes)
//        let
        return getDateDotTimeString(forDate: date.addingTimeInterval(TimeInterval(-60 * lastMapMunutesAgo)))
    }
    
    func getDateDotTimeString(forDate date:Date) -> String {
        return String.composeArrays(elements: dateComponentStrings(components: dateComponents(forDate: date)), separators: ["", "", "."])
    }
    
    func distanceFrom8OnLastDigit(_ number:Int) -> Int {
        return (number+2) % 10
    }
    
    func getProbabilitiesImgUrl() -> URL? {
        let urlString = "https://antistorm.eu/archive/\(getProbabilitiesDateString())-radar-probabilitiesImg.png"
//        "https://antistorm.eu/archive/2019.5.16/22-20-radar-probabilitiesImg.png"
        
        return URL(string: urlString)
    }

    func getProbabilitiesDateString() -> String {
        let calendar = Calendar.current
        let hours = calendar.component(.hour, from: date)
        let minutes = (calendar.component(.minute, from: date) % 10) * 10
//        let lastMapMunutesAgo = distanceFrom8OnLastDigit(minutes)
        let formattedDate = getDateSlashTimeString(forDate: date)
        return "\(formattedDate)\(hours)-\(minutes)"
    }
    
    func getDateSlashTimeString(forDate date:Date) -> String {
        let result = date.description
            .replacingOccurrences(of: "-", with: ".")
            .replacingOccurrences(of: " ", with: "/")
//            .replacingOccurrences(of: ":", with: "-")
        return String(result.prefix(11))
    }
}
