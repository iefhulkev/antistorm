//
//  UrlsRepoResponseParserTests.swift
//  antistormTests
//
//  Created by Grzegorz Moscichowski on 18/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import XCTest

class UrlsRepoResponseParserTests: XCTestCase {

    let mockResponse = "timestamp:1558209001,1558208401,1558207801,1558207202,1558206601,1558206002,1558205401,1558204802,1558204202,1558203602<br>nazwa_folderu:2019.5.18,2019.5.18,2019.5.18,2019.5.18,2019.5.18,2019.5.18,2019.5.18,2019.5.18,2019.5.18,2019.5.18<br>nazwa_pliku:21-50,21-40,21-30,21-20,21-10,21-0,20-50,20-40,20-30,20-20<br>nazwa_pliku_front:20190518.1948,20190518.1938,20190518.1928,20190518.1918,20190518.198,20190518.1858,20190518.1848,20190518.1838,20190518.1828,20190518.1818<br>aktywnosc_burz:1302,1235,1245,1407,1333,1275,1187,1270,1321,1326<br>"
    
    func testExample() {
        let parser = UrlsRepoResponseParser(input:mockResponse)
        let result = parser.extractUrlComponents()
        XCTAssertEqual(result.count, 10)
    }

}
