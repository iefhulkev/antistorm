//
//  UrlsRepositoryTests.swift
//  antistormTests
//
//  Created by Grzegorz Moscichowski on 17/05/2019.
//  Copyright © 2019 Bolder. All rights reserved.
//

import XCTest
import Alamofire

class UrlsRepositoryTests: XCTestCase {

    func testExample() {
        let expectation = XCTestExpectation()
//        let manager = Networking.manager
//        manager.session.serverTrustPolicyManager
//        Networking.manager.request("https://wykop.pl", method: .get).validate().response { (response) in
//            expectation.fulfill()
//
//        }
        
//        let repo = UrlsRepository()
//        Networking.manager.request("https://antistorm.eu/ajaxPaths.php", method: .get, parameters: ["lastTimestamp":0, "type":"radar"]).validate().responseString { (response) in
//            let urlData = UrlsRepoResponseParser.urlsModels(fromResponse: response.result.value ?? "")
////            print(response.result.value ?? "")
//            expectation.fulfill()
//        }

        Alamofire.request("https://antistorm.eu/ajaxPaths.php", method: .get, parameters: ["lastTimestamp":0, "type":"radar"]).validate().responseString { (response) in
            let urlData = UrlsRepoResponseParser.urlsModels(fromResponse: response.result.value ?? "")
            //            print(response.result.value ?? "")
            expectation.fulfill()
        }
        
//        repo.requestUrls{_ in
//
//        }
        wait(for: [expectation], timeout: 10.0)
    }

}
